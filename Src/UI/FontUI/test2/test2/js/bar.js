﻿document.getElementsByClassName = function (className, tag, elm) {
    var testClass = new RegExp("(^|\s)" + className + "(\s|$)");
    var tagtag = tag || "*";
    var elmelm = elm || document;
    var elements = (tag == "*" && elm.all) ? elm.all : elm.getElementsByTagName(tag);
    var returnElements = [];
    var current;
    var length = elements.length;
    for (var i = 0; i < length; i++) {
        current = elements[i];
        if (testClass.test(current.className)) {
            returnElements.push(current);
        }
    }
    return returnElements;
} 

function $(element) {

    return element = document.getElementByClassName(element);
}
function $D() {
    var d = $('content_item');
    var h = d.offsetHeight;
    var maxh = 300;
    function dmove() {
        h += 50; //设置层展开的速度23sc.cn
        if (h >= maxh) {
            d.style.height = '300px';
            clearInterval(iIntervalId);
        } else {
            d.style.display = 'block';
            d.style.height = h + 'px';
        }
    }
    iIntervalId = setInterval(dmove, 2);
}
function $D2() {
    var d = $('content_item');
    var h = d.offsetHeight;
    var maxh = 300;
    function dmove() {
        h -= 50; //设置层收缩的速度23sc.cn
        if (h <= 0) {
            d.style.display = 'none';
            clearInterval(iIntervalId);
        } else {
            d.style.height = h + 'px';
        }
    }
    iIntervalId = setInterval(dmove, 2);
}
function $use() {
    var d = $('content_item');
    var sb = $('content_bar');
    if (d.style.display == 'none') {
        $D();
    } else {
        $D2();
    }
}