﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using DentistClinic.Models;
using System.Security.Principal;
using System.Web.Security;

namespace DentistClinic
{
    // 注意: 有关启用 IIS6 或 IIS7 经典模式的说明，
    // 请访问 http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //Database.SetInitializer(new DropCreateDatabaseAlways<Entities>());    //！！模型改变时重建数据库  慎用！！！会丢掉所有数据

            //Database.SetInitializer(new DentistClinic.Models.Sample());
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            var app = sender as HttpApplication;
            if (app.Context.User != null)
            {
                var user = app.Context.User;
                var identity = user.Identity as FormsIdentity;  // We could explicitly construct an Principal object with roles info using System.Security.Principal.GenericPrincipal                 
                var principalWithRoles = new GenericPrincipal(identity, identity.Ticket.UserData.Split(','));   // Replace the user object                
                app.Context.User = principalWithRoles;
            }
        } 
    }
}