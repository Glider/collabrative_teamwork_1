﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DentistClinic.Models
{
    public  class ServiceType
    {
        public  enum enumServType { Service, Medecine, Ohters };
       public enumServType this[int index]
       {
           get
           {

               switch (index)
               {
                   case (int)enumServType.Service:
                       return enumServType.Service;
                   case (int)enumServType.Medecine:
                       return enumServType.Medecine;
                    default:
                       return enumServType.Ohters;
               }
           }
       }
    }
}