﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DentistClinic.Models
{
    public class ChargeItem
    {
        [Key]
        public int AutoId { get; set; }

        public String strName { get; set; }
        public decimal dPrice { get; set; }
        public String strComment { get; set; }   //备注与说明
        public int nServTypeId { get; set; }    //类型  固化到程序
      
    }
}