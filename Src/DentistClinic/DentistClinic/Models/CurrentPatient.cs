﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DentistClinic.Models
{
    public class CurrentPatient
    {
        [Key]
        public int AutoId { get; set; }
        public String strCreditID{get;set;}
        public DateTime dtEnterTime{get;set;}
        public int nBedNum{get;set;}

    }
}