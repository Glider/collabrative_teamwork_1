﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DentistClinic.Models
{
    public class PatientBasicInfo
    {
        [Key]
        public int AutoId { get; set; }
        public String strCreditID { get; set; }    //身份证
        public DateTime dtPreOderTime { get; set; }
        public int nDiagnoseNum { get; set; }
        public String strPhone { get; set; }
    }
}