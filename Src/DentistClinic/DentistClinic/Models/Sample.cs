﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace DentistClinic.Models
{
    public class Sample:DropCreateDatabaseIfModelChanges<Entities>
    {
        protected override void Seed(Entities context)
        {
         new List<ChargeItem>()
          {
              new ChargeItem(){strName="拔牙",dPrice=100,strComment="拔了个牙",nServTypeId=(int)ServiceType.enumServType.Service },
              new ChargeItem(){strName="上药",dPrice=50,strComment="上了个药",nServTypeId=(int)ServiceType.enumServType.Medecine},
          }.ForEach(a => context.chargeItems.Add(a));

         new List<CurrentPatient>()
          {
              new CurrentPatient(){strCreditID="452124199411123658",dtEnterTime=DateTime.Parse("2012-08-08 00:00:00.000"),nBedNum=5},
              new CurrentPatient(){strCreditID="452124199206206543",dtEnterTime=DateTime.Parse("2013-09-22 00:00:00.000"),nBedNum=4},
              new CurrentPatient(){strCreditID="463256489755562315",dtEnterTime=DateTime.Parse("2013-06-27 00:00:00.000"),nBedNum=3},
              new CurrentPatient(){strCreditID="523236544844451655",dtEnterTime=DateTime.Parse("2013-08-16 00:00:00.000"),nBedNum=2},
              new CurrentPatient(){strCreditID="254655988744451365",dtEnterTime=DateTime.Parse("2013-09-30 00:00:00.000"),nBedNum=1},
              new CurrentPatient(){strCreditID="236565188455565985",dtEnterTime=DateTime.Parse("2013-06-01 00:00:00.000"),nBedNum=7},
          }.ForEach(a => context.currentPatients.Add(a));

         new List<DiagnoseHistory>()
         {
             new DiagnoseHistory(){strCreditID="452124199411123658",dtBeginTime=DateTime.Parse("2012-03-22 00:00:00.000"),dtEndTime=DateTime.Parse("2012-05-22 00:00:00.000"),bCostStatue=true,dTotalCost=222,dArrearage=200,sBedInfo="JB",sRemark="GG"},
             new DiagnoseHistory(){strCreditID="452136255465559855",dtBeginTime=DateTime.Parse("2011-06-01 00:00:00.000"),dtEndTime=DateTime.Parse("2012-03-09 00:00:00.000"),bCostStatue=false,dTotalCost=111,dArrearage=45,sBedInfo="RR",sRemark="RR"},
             new DiagnoseHistory(){strCreditID="236555999865954877",dtBeginTime=DateTime.Parse("2013-01-22 00:00:00.000"),dtEndTime=DateTime.Parse("2013-01-27 00:00:00.000"),bCostStatue=true,dTotalCost=123,dArrearage=50,sBedInfo="QW",sRemark="QWERQ"},
             new DiagnoseHistory(){strCreditID="452362564569987456",dtBeginTime=DateTime.Parse("2013-05-22 00:00:00.000"),dtEndTime=DateTime.Parse("2013-06-01 00:00:00.000"),bCostStatue=true,dTotalCost=456,dArrearage=300,sBedInfo="QWAQAQAER",sRemark="QQQ"},
             new DiagnoseHistory(){strCreditID="542655412365478965",dtBeginTime=DateTime.Parse("2012-08-08 00:00:00.000"),dtEndTime=DateTime.Parse("2013-08-09 00:00:00.000"),bCostStatue=false,dTotalCost=478,dArrearage=350,sBedInfo="QQ",sRemark="RR"}
         }.ForEach(a => context.diagnoseHistories.Add(a));

         new List<MaterialManage>()
          {
              new MaterialManage(){dPrice=100,strComment="一次性注射器",nServTypeId=(int)ServiceType.enumServType.Service ,nCount=200,bState=true},
              new MaterialManage(){dPrice=50,strComment="多次性可交叉感染注射器",nServTypeId=(int)ServiceType.enumServType.Ohters,nCount=45,bState=true},
              new MaterialManage(){dPrice=100,strComment="一次性托盘",nServTypeId=(int)ServiceType.enumServType.Service ,nCount=123,bState=true},
              new MaterialManage(){dPrice=50,strComment="一次性帽子",nServTypeId=(int)ServiceType.enumServType.Ohters,nCount=546,bState=true},
              new MaterialManage(){dPrice=100,strComment="一次性手套",nServTypeId=(int)ServiceType.enumServType.Service ,nCount=345,bState=true},
              new MaterialManage(){dPrice=50,strComment="一次性棉球",nServTypeId=(int)ServiceType.enumServType.Ohters,nCount=156,bState=true},
              new MaterialManage(){dPrice=100,strComment="一次性纱布",nServTypeId=(int)ServiceType.enumServType.Service ,nCount=146,bState=true},
              
          }.ForEach(a => context.materialManages.Add(a));

            new List<PatientBasicInfo>()
            {
                new PatientBasicInfo(){strCreditID="452124199411125654",dtPreOderTime=DateTime.Parse("2012-06-11 00:00:00.000"),nDiagnoseNum=4,strPhone="13163287731"},
                new PatientBasicInfo(){strCreditID="452124199411123658",dtPreOderTime=DateTime.Parse("2013-06-24 00:00:00.000"),nDiagnoseNum=3,strPhone="15977163799"},
                 new PatientBasicInfo(){strCreditID="546236555411152365",dtPreOderTime=DateTime.Parse("2013-01-02 00:00:00.000"),nDiagnoseNum=5,strPhone="13026541235"},
                new PatientBasicInfo(){strCreditID="546236555411152365",dtPreOderTime=DateTime.Parse("2013-07-22 00:00:00.000"),nDiagnoseNum=1,strPhone="13254621533"},
                  new PatientBasicInfo(){strCreditID="452124199307071236",dtPreOderTime=DateTime.Parse("2013-04-04 00:00:00.000"),nDiagnoseNum=7,strPhone="16885885452"},
                 new PatientBasicInfo(){strCreditID="4521243654235554125",dtPreOderTime=DateTime.Parse("2013-06-30 00:00:00.000"),nDiagnoseNum=8,strPhone="13907823366"},
            }.ForEach(a => context.patientBasicInfos.Add(a));

            new List<SystemAccount>()
            {
                new SystemAccount(){strName="lhg",strPassword="admin",strInfo="蓝浩罡",strSection="1",strOther="a"},
                new SystemAccount(){strName="cy",strPassword="admin",strInfo="程应",strSection="2",strOther="s"},
                new SystemAccount(){strName="hh",strPassword="admin",strInfo="胡瀚",strSection="3",strOther="d"},
                new SystemAccount(){strName="lzt",strPassword="admin",strInfo="李振涛",strSection="4",strOther="r"},
                new SystemAccount(){strName="cz",strPassword="admin",strInfo="程洲",strSection="5",strOther="h"},
                new SystemAccount(){strName="tjd",strPassword="admin",strInfo="田江东",strSection="6",strOther="k"},
                new SystemAccount(){strName="sxq",strPassword="admin",strInfo="沈小庆",strSection="7",strOther="l"},
                new SystemAccount(){strName="yxx",strPassword="admin",strInfo="叶晓晓",strSection="8",strOther="m"},
                new SystemAccount(){strName="gxx",strPassword="admin",strInfo="高小娴",strSection="9",strOther="g"},
                
            }.ForEach(a => context.systemAccounts.Add(a));
        }
    }
}