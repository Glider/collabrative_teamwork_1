﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DentistClinic.Models
{
    public class DiagnoseHistory
    {
        [Key]
        public int AutoId { get; set; }
        public String strCreditID { get; set; }
        public DateTime dtBeginTime { get; set; }
        public DateTime dtEndTime { get; set; }
        public bool bCostStatue { get; set; }
        public decimal dTotalCost { get; set; }
        public decimal dArrearage { get; set; }
        public String sBedInfo { get; set; }
        public String sRemark { get; set; }

    }
}