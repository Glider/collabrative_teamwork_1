﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DentistClinic.Models
{
    public class SystemAccount
    {
        [Key]
        public int AutoId { get; set; }
        public String strName { get; set; }
        public String strPassword { get; set; }

        public String strInfo { get; set; }
        public String strSection { get; set; }
        public String strOther { get; set; }
    }
}