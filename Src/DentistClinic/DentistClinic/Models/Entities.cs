﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace DentistClinic.Models
{
    public class Entities:DbContext
    {
        public DbSet<Appointment> appointments { get; set; }
        public DbSet<ChargeItem> chargeItems { get; set; }
        public DbSet<CurrentPatient> currentPatients { get; set; }
        public DbSet<DiagnoseHistory> diagnoseHistories { get; set; }
        public DbSet<MaterialManage> materialManages { get; set; }
        public DbSet<PatientBasicInfo> patientBasicInfos { get; set; }
    
        public DbSet<SystemAccount> systemAccounts { get; set; }
    }
}