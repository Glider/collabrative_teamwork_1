﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DentistClinic.Models
{
    public class Appointment
    {
        [Key]
        public int AutoId { get; set; }
        public String strCreditID { get; set; }
        public DateTime dtAppointTime { get; set; }
        public int nServTypeId { get; set; }
        public String strRemark { get; set; }
        public String strPhone { get; set; }
        public String strOther { get; set; }

    }
}