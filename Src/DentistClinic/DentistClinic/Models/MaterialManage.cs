﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DentistClinic.Models
{
    public class MaterialManage
    {
        [Key]
        public int AutoId { get; set; }
        public int nServTypeId { get; set; }   //物资类型
     
        public String strComment { get; set; }
        public decimal dPrice { get; set; }
        public int nCount { get; set; }
        public bool bState { get; set; }   //状态
    }
}