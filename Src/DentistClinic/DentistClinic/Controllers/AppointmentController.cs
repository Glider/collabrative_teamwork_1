﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentistClinic.Models;

namespace DentistClinic.Controllers
{
    [Authorize]
    public class AppointmentController : Controller
    {
        //内部预约管理控制器
        // GET: /Appointment/
        Entities EN = new Entities();

        public JsonResult Index()
        {
            return Json(EN.appointments, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(Appointment ap)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EN.appointments.Add(ap);
                    EN.SaveChanges();
                    JsonResult res = new JsonResult();
                    res.Data = ap;
                    res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    return res;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
           
        }

        public JsonResult Delete(int AutoId)
        {
            try
            {
                var ap = EN.appointments.Find(AutoId);
                EN.appointments.Remove(ap);
                EN.SaveChanges();
                JsonResult res = new JsonResult();
                res.Data = ap;
                res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                return res;
            }
            catch
            {
                return null;
            }
        }


        public JsonResult Edit(int AutoId, Appointment apm,FormCollection collection)
        {

            var ap = EN.appointments.Find(AutoId);
            if (ModelState.IsValid)
            {
                ap = apm;
            }
            else { return null; }
            try
            {
                if (TryUpdateModel<Appointment>(ap))
                {
                    EN.SaveChanges();
                }
            }
            catch { return null; }
            JsonResult res = new JsonResult();
            res.Data = ap;
            res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return res;
        }

        //通过ID查找，返回json,
        public JsonResult Find(int AutoId)
        {
            try
            {
                var ap = EN.appointments.Find(AutoId);
                return Json(ap, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return null;
            }
        }

        //通过时间区间查找
        public JsonResult FindByTimeRange(DateTime dtStartTime, DateTime dtEndTime)
        {
            var lstApointments=from ap in EN.appointments
                               where (ap.dtAppointTime>=dtStartTime)&&(ap.dtAppointTime<=dtEndTime)
                               select  ap ;
             return Json(lstApointments, JsonRequestBehavior.AllowGet);
        }
    }
}
