﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentistClinic.Models;

namespace DentistClinic.Controllers
{
   [Authorize]
    public class PatientController : Controller
    {
        //患者管理和患者信息控制器
        // GET: /Patient/

        Entities EN = new Entities();

        public JsonResult Index()
        {
            return Json(EN.patientBasicInfos, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///登记患者，并且将其加入当前患者表
        /// </summary>
        /// <param name="mm">PatientBasicInfo pb</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public JsonResult AddPatientInfo(PatientBasicInfo pb)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EN.patientBasicInfos.Add(pb);
                    EN.SaveChanges();
                    JsonResult res = new JsonResult();
                    res.Data = pb;
                    res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    return res;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }

        //public ActionResult Delete(int AutoId)
        //{
        //    var pb = db.patientBasicInfos.Find(AutoId);
        //    db.patientBasicInfos.Remove(pb);
        //    db.SaveChanges();
        //    return View();
        //}


        //public ActionResult Edit(int AutoId, FormCollection collection)
        //{

        //    var pb = db.patientBasicInfos.Find(AutoId);

        //    if (TryUpdateModel<PatientBasicInfo>(pb))
        //    {
        //        db.SaveChanges();
        //    }
        //    return View();
        //}

        //通过时间区间(入院时间)查找当前患者
        public JsonResult FindCurrentByTimeRange(DateTime dtStartTime, DateTime dtEndTime)
        {
            var lstPatients = from cp in EN.currentPatients
                              where (cp.dtEnterTime >= dtStartTime) && (cp.dtEnterTime <= dtEndTime)
                                 select cp;
            return Json(lstPatients, JsonRequestBehavior.AllowGet);
        }

      /////////////////////////患者历史信息处理控制器
        //按状态查找历史
        public JsonResult FindHistoryByState(bool state)
        {
            var lstPatients = from dh in EN.diagnoseHistories
                              where dh.bCostStatue==state
                              select dh;
            return Json(lstPatients, JsonRequestBehavior.AllowGet);
        }

        //通过时间区间(治疗开始时间)查找患者历史
        public JsonResult FindHistoryByStartTimeRange(DateTime dtStartTime, DateTime dtEndTime)
        {
            var lstPatients = from dh in EN.diagnoseHistories
                              where (dh.dtBeginTime >= dtStartTime) && (dh.dtBeginTime <= dtEndTime)
                              select dh;
            return Json(lstPatients, JsonRequestBehavior.AllowGet);
        }

        //通过时间区间(治疗结束时间)查找患者历史
        public JsonResult FindHistoryByEndTimeRange(DateTime dtStartTime, DateTime dtEndTime)
        {
            var lstPatients = from dh in EN.diagnoseHistories
                              where (dh.dtEndTime >= dtStartTime) && (dh.dtEndTime <= dtEndTime)
                              select dh;
            return Json(lstPatients, JsonRequestBehavior.AllowGet);
        }


    }
}
