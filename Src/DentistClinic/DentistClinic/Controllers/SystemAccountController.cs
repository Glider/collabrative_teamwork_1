﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentistClinic.Models;
namespace DentistClinic.Controllers
{
    [Authorize]
    public class SystemAccountController : Controller
    {
        //
        // GET: /SystemAccount/

        //
        // GET: /SystemAccount/
        Entities EN = new Entities();
        public JsonResult Index()
        {
            var res = new JsonResult();
            var SAUsers = EN.systemAccounts.ToList();
            res.Data = SAUsers;
            res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return res;
        }
        
        public JsonResult Add(SystemAccount sa)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    EN.systemAccounts.Add(sa);
                    EN.SaveChanges();
                    JsonResult res = new JsonResult();
                    res.Data = sa;
                    res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    return res;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
           
           
        }

       
        public JsonResult Delete(int AutoId)
        {
            try
            {
                var sa = EN.systemAccounts.Find(AutoId);
                EN.systemAccounts.Remove(sa);
                EN.SaveChanges();
                JsonResult res = new JsonResult();
                res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                res.Data = sa;
                res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                return res;
            }
            catch
            {
                return null;
            }
        }

        
        public JsonResult Edit(int AutoId, SystemAccount sat, FormCollection collection)
        {

            var sa = EN.systemAccounts.Find(AutoId);
            if (ModelState.IsValid)
            {
                sa = sat;
            }
            else { return null; }
            try
            {
                if (TryUpdateModel<SystemAccount>(sa))
                {
                    EN.SaveChanges();
                }
            }
            catch { return null; }
            JsonResult res = new JsonResult();
            res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            res.Data = sa;
            return res;
        }
      
        public JsonResult Find(int AutoId)
        {
            try
            {
                var sa = EN.systemAccounts.Find(AutoId);
                return Json(sa, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return null;
            }

        }

    }
}
