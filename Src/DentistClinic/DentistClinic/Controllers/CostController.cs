﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentistClinic.Models;

namespace DentistClinic.Controllers
{
    [Authorize]
    public class CostController : Controller
    {
        //
        // GET: /Cost/
        Entities EN=new Entities();
        public JsonResult Index()
        {
            return Json(EN.currentPatients,JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(CurrentPatient cp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EN.currentPatients.Add(cp);
                    EN.SaveChanges();
                    JsonResult res = new JsonResult();
                    res.Data = cp;
                    res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    return res;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }

        public ActionResult Delete(int AutoId)
        {
            try
            {
                var cp = EN.currentPatients.Find(AutoId);
                EN.currentPatients.Remove(cp);
                EN.SaveChanges();
                JsonResult res = new JsonResult();
                res.Data = cp;
                res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                return res;
            }
            catch
            {
                return null;
            }
        }


        public ActionResult Edit(int AutoId, CurrentPatient cup,FormCollection collection)
        {

            var cp = EN.currentPatients.Find(AutoId);
            if (ModelState.IsValid)
            {
                cp = cup;
            }
            else { return null; }
            try
            {
                if (TryUpdateModel<CurrentPatient>(cp))
                {
                    EN.SaveChanges();
                }
            }
            catch { return null; }
            JsonResult res = new JsonResult();
            res.Data = cp;
            res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return res;
        }

        //通过ID查找，返回json,
        public JsonResult Find(int AutoId)
        {
            try
            {
                var cp = EN.currentPatients.Find(AutoId);
                return Json(cp, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return null;
            }
        }

    }
}
