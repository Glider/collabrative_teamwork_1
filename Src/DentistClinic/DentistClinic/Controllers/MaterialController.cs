﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentistClinic.Models;

namespace DentistClinic.Controllers
{
    [Authorize]
    public class MaterialController : Controller
    {
        //物资管理控制器
        // GET: /Material/
        Entities EN = new Entities();
        public JsonResult Index()
        {
            return Json(EN.materialManages, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(MaterialManage mm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EN.materialManages.Add(mm);
                    EN.SaveChanges();
                    JsonResult res = new JsonResult();
                    res.Data = mm;
                    res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    return res;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }

        public JsonResult Delete(int AutoId)
        {
            try
            {
                var mm = EN.materialManages.Find(AutoId);
                EN.materialManages.Remove(mm);
                EN.SaveChanges();
                JsonResult res = new JsonResult();
                res.Data = mm;
                res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                return res;
            }
            catch
            {
                return null;
            }
        }


        public JsonResult Edit(int AutoId,MaterialManage mmg, FormCollection collection)
        {

            var mm = EN.materialManages.Find(AutoId);
            if (ModelState.IsValid)
            {
                mm = mmg;
            }
            else { return null; }
            try
            {
                if (TryUpdateModel<MaterialManage>(mm))
                {
                    EN.SaveChanges();
                }
            }
            catch { return null; }
            JsonResult res = new JsonResult();
            res.Data = mm;
            res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return res;
        }

        //通过ID查找，返回json,
        public JsonResult Find(int AutoId)
        {
            try
            {
                var mm = EN.materialManages.Find(AutoId);
                return Json(mm, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return null;
            }
        }
    }
}
