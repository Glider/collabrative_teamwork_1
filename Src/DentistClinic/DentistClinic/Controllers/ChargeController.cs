﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentistClinic.Models;

namespace DentistClinic.Controllers
{   
    [Authorize]
    public class ChargeController : Controller
    {

        Entities EN = new Entities();
        //
        // GET: /Charge/
        public JsonResult Index()
        {
            return Json(EN.chargeItems,JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(ChargeItem chgeItem)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    EN.chargeItems.Add(chgeItem);
                    EN.SaveChanges();
                    JsonResult res = new JsonResult();
                    res.Data = chgeItem;
                    res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                    return res;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
           
           
        }


        public JsonResult Delete(int AutoId)
        {
            try
            {
                var chItem = EN.chargeItems.Find(AutoId);
                EN.chargeItems.Remove(chItem);
                EN.SaveChanges();
                JsonResult res = new JsonResult();
                res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                res.Data = chItem;
                return res;
            }
            catch
            {
                return null;
            }

        }


        public JsonResult Edit(int AutoId, ChargeItem chgeItem, FormCollection collection)
        {

            var chItem = EN.chargeItems.Find(AutoId);
            if (ModelState.IsValid)
            {
                chItem = chgeItem;
            }
            else { return null; }
            try
            {
                if (TryUpdateModel<ChargeItem>(chItem))
                {
                    EN.SaveChanges();
                }
            }
            catch { return null; }
            JsonResult res = new JsonResult();
            res.Data = chItem;
            res.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return res;
            
        }

        //通过ID查找，返回json,
        public JsonResult Find(int AutoId)
        {
            try
            {
                var chItem = EN.chargeItems.Find(AutoId);
                return Json(chItem, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return null;
            }
            
        }
    }
}
