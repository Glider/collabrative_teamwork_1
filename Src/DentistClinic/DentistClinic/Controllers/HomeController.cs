﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentistClinic.Models;
using System.IO;
using System.Text;
using System.Web.Security;

namespace DentistClinic.Controllers
{
    public class HomeController : Controller
    {
        //外部管理控制器，包括预约
        // GET: /Home/
        Entities EN = new Entities();
        public ActionResult Index()
        {
            return Content("Home");
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string u, string p, string returnurl)
        {
            var SAUsers = EN.systemAccounts.ToList();
            for (int i = 0; i < SAUsers.Count; i++)
            {
                if (SAUsers[i].strName.Equals(u) && SAUsers[i].strPassword.Equals(p))
                {
                    string userRoles = "CommonUses";
                    bool isPersistent = true;
                    int version = 1;
                    double persistentMinutes = 60.00; // minutes             
                    string userName = u;
                    string cookiePath = "/";
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(version, userName, DateTime.Now, DateTime.Now.AddMinutes(persistentMinutes), isPersistent, userRoles, cookiePath);
                    string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    Response.Cookies.Add(cookie);
                    break;
                }
                if (i==SAUsers.Count-1)
                {
                    return Content("用户名或密码错误！");
                }
            }
            
            if (string.IsNullOrEmpty(returnurl))
            {
                return Content("你好" + u);
            }
            else
            {
                return Redirect(returnurl);
            }
        }
        public ActionResult Signout()
        {
            FormsAuthentication.SignOut();
            return Redirect("Login");
        }



    }
}
